# RepoMaker
An economical simulation game about makin' repos to save humanity.
Make things cheap as dirt and liberate as many people as you can before a manmade disaster strikes.

[Overview](https://github.com/Houkime/RepoMaker/blob/master/docs/overview)

## Current status

**PROTOTYPE IS UNDER CONSTRUCTION**

* Concept and Math - YES
* Godot project - YES
* Placeholder menu - YES
* Displaying diagrams - YES
* Basic economy calculations (what-if) - YES
    * Initial economy loading - YES
    * Loop mapping - YES
    * Route mapping - YES
    * self-amplification calculations - YES
      * loop assignment - YES
      * loop weights calcs - YES
      * recurrent self-amplificator - YES (it was me to debug)
    * indirect node changes calculations - YES
      * ratio changes calc for self - YES
      * integration with diagram (without folded nodes) - YES
      * interactive testbed for changes calc testing - YES
      * route weighting - YES
      * size and ratio calc for others - YES
      * folded nodes for diagram - YES
      * testing of changes calc - YES
* Economy dynamics - YES
    * Simultaneous changes on one node - YES
    * Category(Mixer) nodes - YES
    * "last ampli" route caching - YES
    * FIX: absolute change (to enable changing links from zero state) - YES
    * Two-level mixers (to enable DIY) - YES
    * profit calculations - YES
    * competitor ratio quickchange - YES
    * User number calculations - YES
      * "producer" users (guys who use your designs industrially)-YES
      * normal end-users - YES
    * barrier nodes - YES
* Timeline - YES
  * Economy ticker - YES
  * timeline GUI - YES
* Player projects - YES
  * Solution lock/unlock - YES
  * Project object and initialization - YES
  * Project versions - YES
* Project dynamics and management. - CURRENT
  * Dev nodes - YES
  * proto-GUI for Projects - BACKGROUND
    * node displaying from repo selection
    * correct usage cost display
  * initial Renown - YES
    * users-to-renown calculations - YES
    * renown impact on dynamics - YES
  * Contributors - YES
    * users-to-contribs ratio - YES
  * Dev users (for example OpenCAD users) - CURRENT
  * trick: Dev node duplication for OSH-industrial
* Load full economy
* Initial balancing
* Prorotype release
* Win/lose/score
* Saving game
* Filling, testing, polish and whatever players want ^_^
