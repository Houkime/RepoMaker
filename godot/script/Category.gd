extends Reference
class_name Category

var name
var color
var initSizes=Array()
var mixerNode
var potentials=Array()

var TRANSIT_SPEED=0.4 #it actually slows down processess
var BARRIER_WEIGHT=0.5 #must be < 1

var shares=Array()
var routeArray=Array()
var barriers = Array()


func calculate_changes():
	shares.clear()
	var sharechanges=Array()
	var sizes=Array()
	var newLinks=Array()
	var linkcount=mixerNode.incomingLinks.size()

	update_barriers()

	for i in range(linkcount):
		sizes.append(initSizes[i]*mixerNode.incomingLinks[i].fromNode.size)

	var sharesum=0

	for i in range(linkcount):
		var share=mixerNode.incomingLinks[i].Percentage/sizes[i]
		shares.append(share)
		sharesum += share

	for i in range(linkcount):
		shares[i] /= sharesum

	for S in shares:
		sharechanges.append(0) # sharechanges init

	for i in range(linkcount): #note: this is a universal code for any number of competitors. It might be too general actually.
		if !mixerNode.incomingLinks[i].fromNode.isLocked:
			for k in range(i,linkcount):
				if (k!=i)&&(!mixerNode.incomingLinks[k].fromNode.isLocked):
					var ik_size_diff = sizes[i] - sizes[k]
					var i_larger_k = ik_size_diff > 0

					var index_of_big = i if i_larger_k else k
					var index_of_small = k if i_larger_k else i

					var RenownRatio = 1 # when users go to an opensource project, we need to take auditory into account.
					var little_project = mixerNode.incomingLinks[index_of_small].fromNode.Project

					if little_project != null:
						RenownRatio = little_project.Renown/Economy.EarthPopulation


					#The idea here is that if a user goes from high capital costs to low capital costs he will actually
					#be able to sell his previous equipment for a new one and may even come out with a desirable profit
					#I am not sure if it is a necessary complication however

					var user_donor_barrier = barriers[index_of_big]
					var user_recipient_barrier = barriers[index_of_small]

					var barrier_sum = user_donor_barrier + user_recipient_barrier
					var barrier_diff = user_donor_barrier - user_recipient_barrier

					var BarrierTransition=exp(sign(barrier_diff)*sqrt(abs(barrier_diff))*BARRIER_WEIGHT*TRANSIT_SPEED)
					var BarrierBurden=exp((-1)*sqrt(barrier_sum)*TRANSIT_SPEED) #is it necessary to count BOTH barriers here?

					var change = abs(ik_size_diff)*shares[index_of_big]*BarrierTransition*BarrierBurden*RenownRatio
					assert(change >= 0)

					sharechanges[index_of_big] -= change
					sharechanges[index_of_small] += change

	for i in range(linkcount):
		shares[i] += sharechanges[i]

	for i in range(linkcount):
		newLinks.append(shares[i]*sizes[i])
	for i in range(linkcount):
		newLinks[i] *= sharesum
	var changearr=Array()
	for i in range(linkcount):
		changearr.append([mixerNode.incomingLinks[i],newLinks[i]-mixerNode.incomingLinks[i].Percentage])

	# make an actual change on ourselves
	mixerNode.change(changearr)
	#(this might be as well in change() code?)
	for N in Economy.nodeArray:
		N.apply_staged_changes()
	Economy.update_loops()

	barriers.clear()

func update_barriers():
	for link in mixerNode.incomingLinks:
		barriers.append(link.fromNode.barrierNode.size)

func _init(nam,col):

	name=nam
	color=col

#	var mixerPar=Array()
#	for E in arr:
#		mixerPar.append([E[0],E[1]])
#		initSizes.append(E[2])
#	var sizesum=0
#	for I in initSizes:
#		sizesum=sizesum+I
#	for i in range(initSizes.size()):
#		initSizes[i]=initSizes[i]/sizesum
#	mixerNode=EcoNode.new(name,mixerPar)
#	mixerNode.isMixer=true
#	var sum=0
#	for E in arr:
#		sum=sum+E[1]*E[2]
#	mixerNode.size=sum
