extends Timer
var Periods=[1,0.5,0.25]

func _init():
	wait_time=Periods[0]

func _ready():
	start()
	paused=true

func _on_Ticker_timeout():
	Economy.update_economy()
