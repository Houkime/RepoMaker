extends Label

class_name LabelWithFont


var font = DynamicFont.new()

var font_path = "res://fonts/ArchivoNarrow-for-Print/ArchivoNarrow-Regular.otf"
var font_size = 20


func _ready():

	var font_data=DynamicFontData.new()
	font_data.font_path = font_path
	font.font_data=font_data
	font.size=font_size

	add_font_override("font",font)
	add_color_override("font_color", ColorN("white"))
