extends Polygon2D

class_name PolyButton

# detailness of curvature
var anglestep=6.28/30
var start
var finish
var SectorName
var numberScale=1.5
var leafScale=1.2
var rootLength=30
var rootline
var percentage
var categoryLabel
var representingEntity


func labelConstructor(numberfont,labelfont):
	var roo=polygon[floor(polygon.size()/2)]
	var ang=(start+finish)/2
	rootline=Line2D.new()
	rootline.add_point(roo)
	var rootLineFinish=roo+rootLength*Vector2(cos(ang),sin(ang))
	rootline.add_point(rootLineFinish)
	rootline.width=1.5

	percentage=Label.new()
	percentage.text=String((floor((finish-start)/2/PI*1000))/10)+"%"
	percentage.add_font_override("font",numberfont)
	add_child(percentage)

	categoryLabel=Label.new()
	categoryLabel.text=SectorName
	categoryLabel.add_font_override("font",labelfont)
	add_child(categoryLabel)

	var leaflength=percentage.rect_size.x
	if (percentage.rect_size.x)>categoryLabel.rect_size.x:
		leaflength=percentage.rect_size.x*leafScale
	else:
		leaflength=categoryLabel.rect_size.x*leafScale
	var leafend=rootLineFinish+Vector2(leaflength*(rootLineFinish.x)/abs(rootLineFinish.x),0)
	rootline.add_point(leafend)

	var tempvec
	tempvec=percentage.rect_size

	if rootLineFinish.x>0:
		tempvec.x=0
	percentage.rect_position=rootLineFinish-tempvec

	categoryLabel.rect_position=leafend
	if rootLineFinish.x>0:
		categoryLabel.rect_position=categoryLabel.rect_position-Vector2(categoryLabel.rect_size.x,0)

	add_child(rootline)

func free_label():
	if rootline!=null:
		rootline.free()
		categoryLabel.free()
		percentage.free()

func _init(rad=100,startang=PI/2,finishang=PI*2,col=ColorN("darkorchid",1),nam="name",obj=null):
	start=startang
	finish=finishang
	if startang==finishang:
		return
	#centerpoint
	var varang=startang
	var pool=PoolVector2Array()
	pool.push_back(Vector2(0,0))
	#arch
	while (varang<=finishang):
		pool.push_back(rad*Vector2(cos(varang),sin(varang)))
		varang=varang+anglestep
	varang=finishang
	pool.push_back(rad*Vector2(cos(varang),sin(varang)))
	color=col
	representingEntity=obj
	polygon=pool
	var outline=Line2D.new()
	pool.push_back(Vector2(0,0))
	outline.points=pool
	outline.default_color=color.lightened(0.5)
	outline.width=1.5
	SectorName=nam
	add_child(outline)




