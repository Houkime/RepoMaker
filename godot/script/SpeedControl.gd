extends Button

var currentSpeed=0
var speedsCount=3
var Signs=Array()
var Ticker

func _ready():
	Ticker=find_parent("MAIN").find_node("Ticker")
	Signs=get_children()


func _on_Speed_button_up():
	currentSpeed=currentSpeed+1
	if currentSpeed>=speedsCount:
		currentSpeed=0
	for i in range(Signs.size()):
		if i==currentSpeed:
			Signs[i].visible=true
		else:
			Signs[i].visible=false
	Ticker.wait_time=Ticker.Periods[currentSpeed]
