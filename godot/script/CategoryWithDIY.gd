class_name CategoryWithDIY
extends Category

func calculate_changes():
	.calculate_changes()
	update_users()

func update_users():
	var usersOfThis=0
	var NeedsRoutes=Economy.NeedsNode.incomingRoutes
	if routeArray.size()==0:
		for R in NeedsRoutes:
			if R.nodeArray.back()==mixerNode:
				routeArray.append(R)
	for R in routeArray:
		var routeUsers=1
		for i in range(R.nodeArray.size()):
			var N=R.nodeArray[i]
			if (N!=mixerNode)&&(N!=Economy.NeedsNode)&&(N.isMixer==true):
				var indexOfNext=N.incomingLinks.find(R.nodeArray[i+1])
				for C in Economy.categoryArray:
					if C.name==N.name:
						routeUsers=routeUsers*C.shares[indexOfNext]
						break
		usersOfThis=usersOfThis+routeUsers-usersOfThis*routeUsers
	# for market nodes
	var subCategory
	for C in Economy.categoryArray:
		if C.name==mixerNode.incomingLinks[0].fromNode.name:
			subCategory=C
			break
	for i in range(subCategory.mixerNode.incomingLinks.size()):
		var N=subCategory.mixerNode.incomingLinks[i].fromNode
		if N.isOpenSource:
			N.users=usersOfThis*Economy.EarthPopulation*shares[0]*subCategory.shares[i]
	# for DIY nodes
	for i in range(1,mixerNode.incomingLinks.size()):
		var N=mixerNode.incomingLinks[i].fromNode
		N.users=usersOfThis*Economy.EarthPopulation*shares[i]

func update_barriers():

	var subCategory
	for C in Economy.categoryArray:
		if C.name==mixerNode.incomingLinks[0].fromNode.name:
			subCategory=C
			break
	var subCatBarSum=0

	for i in range(subCategory.mixerNode.incomingLinks.size()):
		var N=subCategory.mixerNode.incomingLinks[i].fromNode
		subCatBarSum=subCatBarSum+N.barrierNode.size*subCategory.shares[i]

	barriers.append(subCatBarSum)

	for link in mixerNode.incomingLinks.slice(1, -1):
		barriers.append(link.fromNode.barrierNode.size)


func _init(nam,col,marketNodepar,arr).(nam,col):
	var mixerPar=Array()

	mixerPar.append([marketNodepar[0],marketNodepar[1]])
	var marketSize
	for C in Economy.categoryArray:
		if C.name==marketNodepar[0]:
			marketSize=C.mixerNode.size
			C.mixerNode.size=1
			break
	initSizes.append(marketSize)
	for E in arr:
		mixerPar.append([E[0],E[1]])
		initSizes.append(E[2])
		for N in Economy.nodeArray:
			if N.name==E[0]:
				N.isDIY=true
	var sizesum=0
	for I in initSizes:
		sizesum=sizesum+I
	for i in range(initSizes.size()):
		initSizes[i]=initSizes[i]/sizesum

	mixerNode=EcoNode.new(name,mixerPar)
	mixerNode.isMixer=true
