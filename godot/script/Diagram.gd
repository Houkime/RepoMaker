extends Node2D

class_name Diagram

var radius=100

var hoverOffset = 0.1
var contentArray=Array()
var buttonArray=Array()

var hovershade=0.55
var pressshade=0.4

var ActiveButton=null
var PressedButton=null

var numberSize = 28
var labelSize = 23

var numberFont
var labelFont
var colorArray=[
		ColorN("darkorchid"),
		ColorN("green"),
		ColorN("maroon"),
		ColorN("tan"),
		ColorN("midnightblue")

		]

var invisibleButtonLayer
var infolabel
var categoryNodeSelector

var targetNode
var targetCategory
var categoryObjects=Array()

class DiagramEntry:
	var value = 0
	var label = "label"
	var color=ColorN("red")
	var object
	func _init(val,lab,col,obj=null):
		value=val
		label=lab
		color=col
		object=obj


func _init():

#	contentArray=[
#		DiagramEntry.new(40,"Electricity",ColorN("red")),
#		DiagramEntry.new(50,"Construction",ColorN("green")),
#		DiagramEntry.new(60,"Electronics",ColorN("blue")),
#		DiagramEntry.new(20,"Accumulators",ColorN("darkorchid"))
#		]
#
	pass

func _ready():
	var scene = find_parent("MAIN")
	var CONTROL = scene.get_node("Control")

	infolabel = scene.find_node("nodeinfo")
	categoryNodeSelector = scene.find_node("ItemList")

	categoryNodeSelector.connect("item_selected",self,"onReselect")
	targetNode=Economy.nodeArray[0]
	Economy.connect("economy_updated", self, "on_EconomyTick")

	invisibleButtonLayer=TextureButton.new()
	add_child(invisibleButtonLayer)
	invisibleButtonLayer.connect("button_down",self,"onButtonDown")
	invisibleButtonLayer.rect_position=Vector2(radius*-1,radius*-1)
	invisibleButtonLayer.rect_size=Vector2(radius*2,radius*2)

	create_sectors()

func _process(delta):
	var ang = get_local_mouse_position().angle()
	if ang<0:
		ang=2*PI+ang
	var r = get_local_mouse_position().distance_to(Vector2(0,0))

	if r<=radius:
		for b in buttonArray:
			if (ang>=b.start)&&(ang<=b.finish):
				if b!=ActiveButton:
					shade(b,hovershade)
					unshade(ActiveButton,hovershade)
					ActiveButton=b
	elif ActiveButton!=null:
		unshade(ActiveButton,hovershade)
		ActiveButton=null

func flush():
	unshade(ActiveButton,hovershade)
	ActiveButton=null
	contentArray.clear()
	buttonArray.clear()
	for C in get_children():
		if !C==invisibleButtonLayer:
			C.free()

###########################SIGNAL HANDLING#########################

func on_EconomyTick():
	flush()
	categoryNodeSelector.clear()
	categoryObjects.clear()
	create_sectors()


func onButtonDown():
	if ActiveButton!=null&&!Economy.undividableNodes.has(ActiveButton.representingEntity.name):
		var target=ActiveButton.representingEntity
		flush()
		if target.isMixer:
			categoryNodeSelector.visible=true
			targetNode=target.incomingLinks[0].fromNode
			targetCategory=target
		else:
			categoryNodeSelector.visible=false
			targetNode=target
			targetCategory=null
			categoryNodeSelector.clear()
			categoryObjects.clear()
		create_sectors()


func onReselect(id):
	targetNode=categoryObjects[id]
	flush()
	create_sectors()


#################################Coloring#######################

func shade(button,shade, constructorflag=true):
	if button!=null:
		var ang=(button.start+button.finish)/2
		button.color.r=button.color.r*shade
		button.color.g=button.color.g*shade
		button.color.b=button.color.b*shade
		button.position=button.position+radius*hoverOffset*Vector2(cos(ang),sin(ang))
		if constructorflag:
			button.labelConstructor(numberFont,labelFont)


func unshade(button,shade, constructorflag=true):
	if button!=null:
		var ang=(button.start+button.finish)/2
		button.color.r=button.color.r/shade
		button.color.g=button.color.g/shade
		button.color.b=button.color.b/shade
		button.position=button.position-radius*hoverOffset*Vector2(cos(ang),sin(ang))
		if constructorflag:
			button.free_label()

########################Utils############################################

func create_sectors():
	var sum=0
	var startang=0
	var finishang
	var k=0
	for L in targetNode.incomingLinks:
		contentArray.append(DiagramEntry.new(L.Percentage,L.fromNode.name,colorArray[k],L.fromNode))
		k=k+1
	if targetCategory!=null&&categoryNodeSelector.get_item_count()==0:
		for L in targetCategory.incomingLinks:
			if L.fromNode.isMixer:
				for LL in L.fromNode.incomingLinks:
					add_link_to_selector(LL)
			else:
				add_link_to_selector(L)
		categoryNodeSelector.select(0)
	infolabel.text=targetNode.name+" "+String(targetNode.size)
	for i in contentArray:
		sum=sum+i.value
	for i in contentArray:
		finishang=startang+2*PI*i.value/sum
		var PolyInstance=PolyButton.new(radius,startang,finishang,i.color,i.label,i.object)
		add_child(PolyInstance)
		buttonArray.append(PolyInstance)
		startang=finishang
		var FontData=load("res://fonts/ArchivoNarrow.tres")
		numberFont=DynamicFont.new()
		numberFont.font_data=FontData
		numberFont.size=numberSize
		labelFont=DynamicFont.new()
		labelFont.font_data=FontData
		labelFont.size=labelSize

func add_link_to_selector(L):
	if !L.fromNode.isLocked:
		var users=floor(L.fromNode.producers+L.fromNode.users)
		categoryNodeSelector.add_item(L.fromNode.name+" ("+String(users)+" users)")
		categoryObjects.append(L.fromNode)
