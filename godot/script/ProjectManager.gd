extends Node2D

var NameLabel
var selectedNode

func _init():
	add_user_signal("node_selected")
func _ready():
	NameLabel=get_node("Naming")


func select(nodeRepresenter):
	NameLabel.text=nodeRepresenter.NodeName
	selectedNode=nodeRepresenter
	update_startStatus()
	get_node("UnstartedProject/VBoxContainer/Description").text=nodeRepresenter.Project.description
	emit_signal("node_selected")

func update_startStatus():
	var act = selectedNode.Project.isActive
	get_node("StartedProject").visible=act
	get_node("UnstartedProject").visible=!act
