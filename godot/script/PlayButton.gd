extends Button

var speedsCount=2
var currentSpeed=0
var Signs=Array()
var Ticker

func _ready():
	Ticker=find_parent("MAIN").find_node("Ticker")
	Signs=get_children()

func _on_PlayButton_button_up():

	currentSpeed=currentSpeed+1
	if currentSpeed>=speedsCount:
		currentSpeed=0
	for i in range(Signs.size()):
		if i==currentSpeed:
			Signs[i].visible=true
		else:
			Signs[i].visible=false

	Ticker.paused=!Ticker.paused

