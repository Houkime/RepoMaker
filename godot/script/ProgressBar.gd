extends Node2D

var Hmargin=8.5
var VScale=0.7
var PolyBar
var BarBed
var day
var maxDay
var hairColor=ColorN("white").darkened(0.5)
var hairWidth=1
var thickHairWidth=2
var hairScale=1.1

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	Economy.connect("economy_updated",self,"onEconomyUpdate")
	PolyBar=Polygon2D.new()
	add_child(PolyBar)
	PolyBar.z_index=1
	BarBed=get_parent()
	day=Economy.currentDay
	maxDay=Economy.MassiveDestructionDay
	add_hairmarks()

func add_hairmarks():

	var width=BarBed.rect_size.y*VScale*hairScale
	var VMargin=BarBed.rect_size.y*(1-VScale*hairScale)/2

	var markCount=floor(maxDay/365)
	var root=Vector2(Hmargin,VMargin)
	for i in range(markCount):
		var line=Line2D.new()
		line.add_point(root)
		line.add_point(root+Vector2(0,width))
		if i-(floor(i/5)*5)==0:
			line.width=thickHairWidth
		else:
			line.width=hairWidth
		line.default_color=hairColor
		add_child(line)
		root=root+Vector2((BarBed.rect_size.x-2*Hmargin)/markCount,0)

func onEconomyUpdate():
	day=Economy.currentDay
	var pool=Array()
	var width=BarBed.rect_size.y*VScale
	var VMargin=BarBed.rect_size.y*(1-VScale)/2
	pool.push_back(Vector2(Hmargin,VMargin))
	pool.push_back(pool.back()+Vector2(0,width))
	pool.push_back(pool.back()+Vector2(BarBed.rect_size.x*day/maxDay,0))
	pool.push_back(pool.back()+Vector2(0,-width))
	PolyBar.polygon=pool
