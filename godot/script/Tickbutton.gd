extends Button

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	pass

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass


func _on_Button4_button_up():
	var scene=find_parent("MAIN")
	var Diagram=get_node("/root/Node2D/Control/Diagram center")
	var categoryNodeSelector=get_node("/root/Node2D/Control/ItemList")
	for C in Diagram.Economy.categoryArray:
		C.calculate_changes()
	Diagram.flush()
	categoryNodeSelector.clear()
	Diagram.categoryObjects.clear()
	Diagram.create_sectors()
func _init():
 pass
