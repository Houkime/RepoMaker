extends Node2D

export var NodeName="name"
var anglestep=6.28/30
var Body
var hovershade=0.5
var radius=30
var progressRadius=40
var textureLayer
var progressCircle
var selectionCircle
var selectionRadius=50
var Selected=false
var Project
var ignoreUnselection=false
var ProjectManager

func _ready():
	var MAIN = find_parent("MAIN")
	ProjectManager=MAIN.find_node("ProjectManager")

	ProjectManager.connect("node_selected",self,"onUnselection")
	selectionCircle=create_polygon(-2,selectionRadius,0,2*PI,ColorN("cyan"))
	add_child(selectionCircle)
	selectionCircle.visible=false
	for P in Economy.projectArray:
		if P.name==NodeName:
			Project=P
			break
	Economy.connect("economy_updated", self, "on_EconomyTick")
	Body=create_polygon(0,radius)
	add_child(Body)
	textureLayer=TextureButton.new()
	add_child(textureLayer)
	textureLayer.connect("mouse_entered",self,"onMouseEnter")
	textureLayer.connect("mouse_exited",self,"onMouseExit")
	textureLayer.connect("button_down",self,"onButtonDown")
	var Tex=load("res://node_icons/"+NodeName+".svg")
	textureLayer.texture_normal=Tex
	textureLayer.expand=true
	textureLayer.rect_position=Vector2(radius*-1,radius*-1)
	textureLayer.rect_size=Vector2(radius*2,radius*2)

func onButtonDown():
	Selected=true
	selectionCircle.visible=true
	ignoreUnselection=true
	ProjectManager.select(self)

func onUnselection():
	if ignoreUnselection:
		ignoreUnselection=false
		return
	if Selected:
		Selected=false
		selectionCircle.visible=false


func on_EconomyTick():
	if progressCircle!=null:
		progressCircle.free()
	var contribs=Project.contributions
	var toNext= Project.ToNextVersion
	var finishang=2*PI*contribs/toNext
	progressCircle=create_polygon(-1,progressRadius,0,finishang,ColorN("green"))
	if !progressCircle==null:
		add_child(progressCircle)

func onMouseEnter():
	Body.color=Body.color.darkened(0.5)

func onMouseExit():
	Body.color=Body.color.lightened(0.5)

func create_polygon(z,rad=100,startang=0,finishang=PI*2,col=ColorN("gray",1)):

	if startang==finishang:
		return
	#centerpoint
	var varang=startang
	var pool=PoolVector2Array()
	pool.push_back(Vector2(0,0))
	#arch
	while (varang<=finishang):
		pool.push_back(rad*Vector2(cos(varang),sin(varang)))
		varang=varang+anglestep
	varang=finishang
	pool.push_back(rad*Vector2(cos(varang),sin(varang)))
	var Poly=Polygon2D.new()
	Poly.color=col
	Poly.polygon=pool
	var outline=Line2D.new()
	pool.remove(0)
	outline.points=pool
	outline.default_color=Poly.color.lightened(0.5)
	outline.width=1.5
	Poly.z_index=z
	outline.z_index=z
	Poly.add_child(outline)
	return Poly
