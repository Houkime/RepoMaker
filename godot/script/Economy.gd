extends Node

var EarthPopulation=1000000

var nodeArray=Array()
var linkArray=Array()
var loopArray=Array()
var categoryArray=Array()
var projectArray=Array()
var undividableNodes=[
		"people"
] #by name
var GatherNode
var NeedsNode
var initialEmployment= 10 #in hours
var currentDay=0
var MassiveDestructionDay=20*365
var auditoryPerUser=10

signal economy_updated

func nodefill():
	nodeArray.append(
			EcoNode.new(
				"Needs",
				[
					["rect2", 0.3],
					["isle9", 0.3],
					["north12", 0.4]

				]))
	nodeArray.back().isDIY=true

	nodeArray.append(
			EcoNode.new(
				"rect1",
				[
					["rect2", 0.3],
					["rect4", 0.3],
					["people", 0.4]

				]))
	nodeArray.append(
			EcoNode.new(
				"originalrect2",
				[
					["rect3", 0.3],
					["rect1", 0.3],
					["people", 0.4]

				]))
	nodeArray.append(
			EcoNode.new(
				"rect3",
				[
					["rect2", 0.3],
					["rect4", 0.3],
					["people", 0.4]

				]))
	nodeArray.append(
			EcoNode.new(
				"rect4",
				[
					#["tree5", 0],
					["rect1", 0.3],
					["rect3", 0.3],
					["people", 0.4]

				]))
	nodeArray.append(
			EcoNode.new(
				"altrect2",
				[
					#["tree5", 0],
					["rect1", 0.6],
					["people", 0.4]

				]))
	nodeArray.back().isOpenSource=true
	nodeArray.back().isLocked=true
	nodeArray.append(
			EcoNode.new(
				"otherrect2",
				[
					#["tree5", 0],
					["rect1", 0.5],
					["rect3", 0.5]

				]))
	nodeArray.append(
			EcoNode.new(
				"DIYrect2",
				[
					#["tree5", 0],
					["rect1", 0.3],
					["rect3", 0.7]

				]))
	nodeArray.append(
			EcoNode.new(
				"DIYrect2_barrier",
				[
					#["tree5", 0],
					["rect1", 0.3],
					["rect3", 0.7]

				]))
	nodeArray.back().isDIY=true
	nodeArray.back().size=8

	nodeArray.append(
			EcoNode.new(
				"otherrect2_barrier",
				[
					#["tree5", 0],
					["rect1", 0.5],
					["rect3", 0.5]

				]))
	nodeArray.back().size=20

	nodeArray.append(
			EcoNode.new(
				"altrect2_barrier",
				[
					#["tree5", 0],
					["rect1", 0.6],
					["people", 0.4]

				]))
	nodeArray.back().size=5

	nodeArray.append(
			EcoNode.new(
				"altrect2_dev",
				[
					#["tree5", 0],
					["rect1", 0.6],
					["people", 0.4]

				]))
	nodeArray.back().size=7

	nodeArray.append(
			EcoNode.new(
				"originalrect2_barrier",
				[
					["rect3", 0.3],
					["rect1", 0.3],
					["people", 0.4]
				]))
	nodeArray.back().size=13

	nodeArray.append(
			EcoNode.new(
				"tree5",
				[
					["tree6", 1/3],
					["rect4", 1/3],
					["tree7", 1/3]

				]))
	nodeArray.append(
			EcoNode.new(
				"tree6",
				[
				]))

	nodeArray.append(
			EcoNode.new(
				"tree7",
				[
					["tree5", 0.5],
					["people", 0.5]
				]))
	nodeArray.append(
			EcoNode.new(
				"isle8",
				[
					["rect3", 1/3],
					["isle9", 1/3],
					["isle10", 1/3]

				]))
	nodeArray.append(
			EcoNode.new(
				"isle9",
				[
					["isle8", 1/2],
					["isle10", 1/2]

				]))
	nodeArray.append(
			EcoNode.new(
				"isle10",
				[
					["isle9", 1/2],
					["isle8", 1/2]

				]))
	nodeArray.append(
			EcoNode.new(
				"hermit11",
				[
					["hermit11", 0.15],
					["isle9", 0.75]

				]))
	nodeArray.append(
			EcoNode.new(
				"north12",
				[
					["hermit11", 1/3],
					["north14", 1/3],
					["north13", 1/3]

				]))
	nodeArray.append(
			EcoNode.new(
				"north13",
				[
					["rect1", 1/3],
					["north14", 1/3],
					["north12", 1/3]

				]))
	nodeArray.append(
			EcoNode.new(
				"north14",
				[
					["north12", 0.5],
					["north13", 0.5]

				]))
	nodeArray.append(
			EcoNode.new(
				"tree15",
				[
					["tree17", 1]


				]))
	nodeArray.append(
			EcoNode.new(
				"tree16",
				[
					["tree17", 1]

				]))
	nodeArray.append(
			EcoNode.new(
				"tree17",
				[
					["rect4", 1]

				]))
	nodeArray.append(
			EcoNode.new(
				"onedir18",
				[
					["hermit11", 1/2],
					["onedir20", 1/2]

				]))
	nodeArray.append(
			EcoNode.new(
				"onedir19",
				[
					["onedir18", 1]

				]))
	nodeArray.append(
			EcoNode.new(
				"onedir20",
				[
					["onedir19", 1]

				]))
	nodeArray.append(
			EcoNode.new(
				"people",
				[

				]))

func categoryfill():
	categoryArray.append(
			MarketCategory.new(
				"rect2",
				ColorN("orange"),
				[ #[nodename,initial share,initial size (only ratio matters)
					["originalrect2",0.8,4.23],
					["altrect2",0,2.2],
					["otherrect2",0.2,1.1]
				]))
	categoryArray.append(
			CategoryWithDIY.new(
				"rect2_full",
				ColorN("orange"),
				["rect2",0.8],
				[ #[nodename,initial share,initial size (only ratio matters)
					["DIYrect2",0.2,2.2]
				]))
	for c in categoryArray:
		nodeArray.append(c.mixerNode)
func projectfill():
	projectArray.append(
			PlayerProject.new("altrect2"))

##################################TOPOLOGY#########################

func topology_mapper():
	var topologyHistory=Array()
	for N in nodeArray:
		GatherNode.tempLinks[0].fromNode=N
		var link=N.tempLinks[0]
		N.tempLinks.clear()
		topology_bot(N,topologyHistory)
		N.tempLinks.append(link)
		topologyHistory.clear()

func topology_bot(node,history):
	#print(node.name)
	if node==GatherNode:
		GatherNode.tempLinks[0].fromNode.incomingRoutes.append(Route.new(history))
		#should it be inverted?
		return

	if history.has(node):
		var index=history.find(node)
		var loopNodes=Array()
		var i=history.size()-1
		var h
		while i>=index:
			loopNodes.append(history.pop_back())
			i=history.size()-1
		#loopNodes.invert()
		h=rotary_hash(loopNodes)
		var has=false
		for L in loopArray:
			if L.hashsum==h:
				has=true
				break
		if !has:
			loopArray.append(Loop.new(loopNodes,h))
		return

	history.append(node)

	for l in node.incomingLinks:
		topology_bot(l.fromNode,history.duplicate())
	for l in node.tempLinks:
		topology_bot(l.fromNode,history.duplicate())

#Loop comparison. Must return same result for rotated loops,
# but different results for loops in different directions
func rotary_hash(arr):
	var hasharr= Array()
	for N in arr:
		hasharr.append(rand_seed(N.name.hash())[1])
	#symmetry-broken hash
	#print(hasharr)
	var rotor=0
	for i in range(hasharr.size()):
		rotor=rotor+hasharr[i]*(hasharr[i-1]^2)
	return rotor


##############################UPDATES#####################


func update_loops():
		for L in loopArray:
			L.calculate_weight()


func update_economy():
	for C in categoryArray:
		C.calculate_changes()
	currentDay=currentDay+1
	for P in projectArray:
		P.tick()
	emit_signal("economy_updated")


##########################INIT##################################

func _init(): #bootstrap 0
	# we need to return an object before full init
	# otherwise it will be recurrent
	call_deferred("bootstrap")

func bootstrap():
	print("initting economy")
	nodefill()
	categoryfill()
	for N in nodeArray:
		N.create_links()

	GatherNode=EcoNode.new("Gatherer",["unimpotantname",0])
	#DummyLink
	GatherNode.tempLinks.append(Link.new(GatherNode,GatherNode,0))
	#nodeArray.append(EcoNode)

	for N in nodeArray:
		N.tempLinks.append(Link.new(N,GatherNode,0))

	topology_mapper()
	#Careful here. Don't know whether it really works!
	for N in nodeArray:
		N.tempLinks.clear()
	GatherNode.tempLinks.clear()
	GatherNode=null
	for L in loopArray:
		L.applicate_to_nodes()
		L.applicate_to_links()
		L.calculate_weight()
	for N in nodeArray:
		for R in N.incomingRoutes:
			R.applicate_to_links()
			R.calculate_weight()
	for N in nodeArray:
		if N.name=="Needs":
			NeedsNode=N
			N.size=initialEmployment
			break
	#in order to calculate shares
	for C in categoryArray:
		C.mixerNode.change([[C.mixerNode.incomingLinks[0],0]])
	projectfill()
	emit_signal("economy_updated")

#	for N in nodeArray:
#		print(N.name)
#		for L in N.loops:
#			print("\tLoop")
#			for N in L.nodeArray:
#				print("\t\t",N.name)
#	for N in nodeArray:
#		N.calculate_selfAmplificator(Array())
#	print("SELF-AMPLI = ", nodeArray[0].calculate_selfAmplificator(Array(),true))

#	for N in nodeArray:
#		print(N.name)
#		for R in N.incomingRoutes:
#			print("\t Route")
#			for NN in R.nodeArray:
#				print("\t\t",NN.name)
#
#	for L in linkArray:
#		print("")
#		print(L.toNode.name)
#		print(L.fromNode.name)
#		print(L.Percentage)
#	for L in loopArray:
#		print("Loop")
#		for N in L.nodeArray:
#			print("\t",N.name)
#		print(L.weight)
