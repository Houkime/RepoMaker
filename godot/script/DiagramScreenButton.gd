extends ButtonOfMenu

onready var PROJECT_SCREEN = find_parent("MAIN").find_node("PROJECT_SCREEN")
onready var DIAGRAM_SCREEN = find_parent("MAIN").find_node("DIAGRAM_SCREEN")

enum state {
	PROJECT,
	DIAGRAM
}

var DIAGRAM_TEXT = "Diagram"
var PROJECTS_TEXT = "Projects"

var state_names = {
	state.PROJECT: PROJECTS_TEXT,
	state.DIAGRAM: DIAGRAM_TEXT,
}

var current_mode = state.PROJECT

func _ready():
	to_project()
	update_label()

func to_diagram():
	show_project(true)
	show_diagram()

func to_project():
	show_project()
	show_diagram(true)

func show_project(hide = false):
	PROJECT_SCREEN.visible = true if !hide else false

func show_diagram(hide = false):
	DIAGRAM_SCREEN.visible = true if !hide else false

func on_click():
	if current_mode == state.PROJECT:
		to_diagram()
	else:
		to_project()
	current_mode = (current_mode+1) % state.size()
	update_label()

func update_label():
	var next_mode = (current_mode+1) % state.size()
	label.text = state_names[next_mode]
