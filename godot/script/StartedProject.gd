extends Node2D

func _ready():
	update_stats()
	Economy.connect("economy_updated",self,"onEconomyUpdate")
	connect("visibility_changed",self,"update_stats")

func onEconomyUpdate():
	update_stats()

func _init():
	visible=false

func update_stats():
	if visible:
		var Project=($"..").selectedNode.Project
		$VBoxContainer/Users.text="Users: "+present_number(Project.totalUsers)
		$VBoxContainer/Contribs.text="Contribs: "+present_number(Project.contribs)
#		$VBoxContainer/Usage.text="Usage cost%: "+String(Project.totalUsers) #What exactly is better to display here?
#		$VBoxContainer/Adoption.text="Adoption cost: "+String(Project.totalUsers)
#		$VBoxContainer/Dev.text="Dev cost: "+String(Project.totalUsers)
		$VBoxContainer/Auditory.text="Auditory: "+present_number(Project.Renown)
		$VBoxContainer/Commits.text="Commits/day: "+present_number(Project.contributionsPerDay)


func present_number(number):
	var string
	number=float(floor(number))
	if number>2000:
		string = String(floor(number/1000*10)/10.0)+"K"
	else:
		string=String(number)
	return string
