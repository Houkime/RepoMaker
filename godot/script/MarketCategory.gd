extends Category
class_name MarketCategory


func calculate_changes():
	.calculate_changes()
	update_producers()

func update_producers():
	var sum=0
	var NeedsRoutes=Economy.NeedsNode.incomingRoutes
	if routeArray.size()==0:
		for R in NeedsRoutes:
			if R.nodeArray.back()==mixerNode:
				routeArray.append(R)
	for R in routeArray:
		sum=sum+R.lastampli
	for L in mixerNode.incomingLinks:
		var prod=L.Percentage*Economy.EarthPopulation*sum
		var perc=L.Percentage
		var pop=Economy.EarthPopulation
		L.fromNode.producers=prod
#		print(L.fromNode.name," ", L.fromNode.producers)

func _init(nam,col,arr).(nam,col):

	var mixerPar=Array()
	for E in arr:
		mixerPar.append([E[0],E[1]])
		initSizes.append(E[2])
	var sizesum=0
	for I in initSizes:
		sizesum=sizesum+I
	for i in range(initSizes.size()):
		initSizes[i]=initSizes[i]/sizesum
	mixerNode=EcoNode.new(name,mixerPar)
	mixerNode.isMixer=true
	var sum=0
	for E in arr:
		sum=sum+E[1]*E[2]
	mixerNode.size=sum

