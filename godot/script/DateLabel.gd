extends LabelWithFont

func _ready():
	Economy.connect("economy_updated",self,"onEconomyUpdate")
	text="0.0.0"

func onEconomyUpdate():
	var day=Economy.currentDay
	var year=floor(day/360)
	day=day-year*360
	var month=floor(day/30)
	day=day-month*30
	text=String(day)+"."+String(month)+"."+String(year)
