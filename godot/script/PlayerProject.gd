var name
var node
var barrierNode
var devNode
var Upgrader
var description
var version=-1
var contributions=0
var ToNextVersion=0
var isMaxed=false
var isActive=false
var totalUsers=0
var normUsers=0
var productionUsers=0
var devUsers=0
var Renown=0 #number of people who know about our stuff
var contributionsPerDay=1
var contribs=1
var contribsPerUser=0.1
var contribPower=1 #how many commits/day/contrib?
var PrototypeRenownBonus=100 #Prototype is finished and our repo is discovered by scout.
var renBonus=0

class_name PlayerProject

func _init(nam):
	name=nam
	for N in Economy.nodeArray:
		if N.name==name:
			node=N
			N.Project=self
			break
	for N in Economy.nodeArray:
		if N.name==name+"_barrier":
			barrierNode=N
			break
	for N in Economy.nodeArray:
		if N.name==name+"_dev":
			devNode=N
			break
	Upgrader=load("res://script/project_version_trees/"+name+".gd").new()
	ToNextVersion=Upgrader.versionCosts[0]
	description=Upgrader.description

func upgrade():
	if !isMaxed:
		version=version+1
		if version==0:
			node.isLocked=false
			renBonus+=PrototypeRenownBonus
		else:
			Upgrader.upgrade(self,version)
		if version==(Upgrader.versionCosts.size()-1):
			isMaxed=true
		if !isMaxed:
			ToNextVersion=Upgrader.versionCosts[version+1]
	contributions=0

func tick():
	if !isMaxed&&isActive:
		contributions+=contributionsPerDay
		if contributions>=ToNextVersion:
			upgrade()
			contributions=0
	productionUsers=node.producers
	normUsers=node.users
	totalUsers=normUsers+productionUsers
	Renown=Economy.EarthPopulation*(1-exp(-Economy.auditoryPerUser*totalUsers/Economy.EarthPopulation))
	Renown+=renBonus
	contribs=1+totalUsers*contribsPerUser
	contributionsPerDay=contribs*contribPower
