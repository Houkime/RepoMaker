extends Node2D

class_name ButtonOfMenu

export var text = "some text"

var COLOR_SHADED = Color8(255,255,255,255)
var COLOR_UNSHADED = Color8(50,50,50,255)

var FONT_COLOR_SHADED = Color8(50,50,50,255)
var FONT_COLOR_UNSHADED = Color8(255,255,255,255)

var WIDTH = 200
var HEIGHT = 50

var bg_rect = ColorRect.new()
var invisible_button = TextureButton.new()
var label = LabelWithFont.new()


func hover(dehover = false):
	bg_rect.color = COLOR_SHADED if !dehover else COLOR_UNSHADED
	label.modulate = FONT_COLOR_SHADED if !dehover else FONT_COLOR_UNSHADED

func _ready():
	bg_rect.color = COLOR_UNSHADED

	for r in [bg_rect,invisible_button,label]:
		r.rect_size = Vector2(WIDTH,HEIGHT)
		add_child(r)

	invisible_button.connect("mouse_entered",self,"on_hover")
	invisible_button.connect("mouse_exited",self,"on_dehover")
	invisible_button.connect("button_up",self,"on_click")

	label.text = text
	label.align = Label.ALIGN_CENTER
	label.valign = Label.ALIGN_CENTER

func on_hover():
	hover(false)

func on_dehover():
	hover(true)
